package by.step.it.library.j2018.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcHelper {
	private String uri;
	private String dataBaseName;
	private String userName;
	private String password;
	private static JdbcHelper instance;
	
	private JdbcHelper() {
		super();
	}
	
	public JdbcHelper configure(String uri, String dataBaseName,
			String userName, String password) {
		this.uri = uri;
		this.dataBaseName = dataBaseName;
		this.userName = userName;
		this.password = password;
		return this;
	}
	
	public boolean connect() {
		String urlString = String.
				format("jdbc:mysql://%s/%s?autoReconnect=true&useSSL=false",
						this.uri, this.dataBaseName);
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager
					.getConnection(this.uri, this.userName,
							this.password);
		} catch (ClassNotFoundException | SQLException e) {
			return false;
		}
		
		return false;
	}
	
	public static JdbcHelper getInstance() {
		synchronized (instance) {
			if (instance == null) {
				instance = new JdbcHelper();
			}
		}
		return instance;
	}
}
