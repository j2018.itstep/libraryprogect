package by.step.it.library.j2018.data;

import java.util.List;

import by.step.it.library.j2018.model.Author;
import by.step.it.library.j2018.model.Book;
import by.step.it.library.j2018.model.Genre;

public interface Repository {
	boolean addBook(Book book);
	boolean addAuthor(Author author);
	List<Genre> getAllGenres();
	List<Book> getAllBooks();
	List<Book> getBooksByGenre(Genre genre);
	List<Book> getBooksByAuthor(Author author);
	Book getBookById(long id);
	List<Author> getAllAuthors();
	List<Author> getAuthorsByName(String firstName,
			String lastName, String alias);
	Author getAuthorById(long id);
}
