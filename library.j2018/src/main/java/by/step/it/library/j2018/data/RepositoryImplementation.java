package by.step.it.library.j2018.data;

import java.util.List;

import by.step.it.library.j2018.model.Author;
import by.step.it.library.j2018.model.Book;
import by.step.it.library.j2018.model.Genre;

public class RepositoryImplementation implements Repository{
	
	@Override
	public boolean addBook(Book book) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAuthor(Author author) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Genre> getAllGenres() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Book> getAllBooks() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Book> getBooksByGenre(Genre genre) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Book> getBooksByAuthor(Author author) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Book getBookById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Author> getAllAuthors() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Author> getAuthorsByName(String firstName, String lastName, String alias) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Author getAuthorById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
