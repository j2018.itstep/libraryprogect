package by.step.it.library.j2018;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class App {
	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager
					.getConnection("jdbc:mysql://localhost:3306/world?autoReconnect=true&useSSL=false", "root", "admin");
//			System.out.println(connection.isValid(100));

			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.
					executeQuery("SELECT country.Name, country.Code, "
							+ "country.Continent, country.Population " + 
					"FROM country " + 
					"WHERE country.Name LIKE 'J%';");
			while (resultSet.next()) {
				Country country = new Country();
				country.setName(resultSet.getString(1));
				country.setCode(resultSet.getString(2));
				country.setContinent(resultSet.getString(3));
				country.setPopulation(resultSet.getLong(4));
				System.out.println(country);
			}
			connection.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
